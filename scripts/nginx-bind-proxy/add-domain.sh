#!/bin/bash

if [ $# != 1 ]; then
        echo "Usage example:";
        echo "$0 domain.com";
        exit 2;
fi
DOMAIN=$1

rm -rf /tmp/add-domain
mkdir /tmp/add-domain

cp /root/scripts/templates/bind-zone /tmp/add-domain/bind-zone
cp /root/scripts/templates/bind-include /tmp/add-domain/bind-include
cp /root/scripts/templates/nginx-vhost /tmp/add-domain/nginx-vhost

sed -i -e "s/##DOMAIN##/$DOMAIN/g" /tmp/add-domain/bind-zone
sed -i -e "s/##DOMAIN##/$DOMAIN/g" /tmp/add-domain/bind-include
sed -i -e "s/##DOMAIN##/$DOMAIN/g" /tmp/add-domain/nginx-vhost

#echo -e "\n" >> /etc/bind/named.conf.local
cat /tmp/add-domain/bind-include >> /etc/bind/named.conf.local
cp /tmp/add-domain/bind-zone /etc/bind/external/$DOMAIN
cp /tmp/add-domain/nginx-vhost /etc/nginx/sites-available/$DOMAIN
ln -s /etc/nginx/sites-available/$DOMAIN /etc/nginx/sites-enabled/100-$DOMAIN

service nginx reload
service bind9 reload
